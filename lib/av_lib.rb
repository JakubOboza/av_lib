require "av_lib/version"
require "av_lib/response"
require "av_lib/request"
require 'rest-client'
# regular request
# dumb version to refactor

module AvLib

  extend self

  @@config = {
    host: "localhost",
    port: 80,
    api_key: ""
  }

  def setup(params = {})
    @@config.merge!(params)
  end

  def api_key
    @@config[:api_key]
  end

  def host
    "#{@@config[:host]}:#{@@config[:port]}"
  end

  def operator_from_param(operator)
    {
      three:    "three-uk",
      tmobile:  "t-mobile-uk",
      orange:   "orange-uk",
      o2:       "o2-uk",
      dummy:    "dummy",
      vodafone: "vodafone-uk",
      ee:       "ee"
    }[operator]
  end

  def lookup(operator, msisdn, explode_on_error = false)
    begin
      response = Request.new(self.host, self.api_key, self.operator_from_param(operator), msisdn).fetch
      return ResponseBodyParser.new(response.body).parse
    rescue RestClient::RequestTimeout => e
      if explode_on_error
        raise e
      end
      return TimeOutResponse.new(e)
    end
  end

end

