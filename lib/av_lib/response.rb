require 'nokogiri'

module AvLib

  class Response

    attr_reader :status, :operator, :msisdn

    def initialize(status, operator, msisdn)
      @status = status.to_s.strip
      @operator = operator
      @msisdn = msisdn
    end

    def verified?
      if @status == "true"
        return true
      elsif @status == "false"
        return false
      else
        "unknown"
      end
    end

    def status
      @status
    end

  end

  class TimeOutResponse

    attr_reader :exception

    def initialize(exception)
      @exception = exception
    end

    def verified?
      "unknown"
    end
  end


  class ResponseBodyParser

    attr_reader :body

    def initialize(body)
      @body = body
    end

    def parse
      parsed_body = Nokogiri::HTML(@body)
      av_status = parsed_body.css("dd")[2].content
      msisdn    = parsed_body.css("dd")[1].content
      operator  = parsed_body.css("dd")[0].content
      return Response.new(av_status, operator, msisdn)
    end

  end


end