module AvLib

  class Request

    attr_reader :host, :api_key, :operator, :msisdn

    def initialize(host, api_key, operator, msisdn)
      @host = host
      @api_key = api_key
      @operator = operator
      @msisdn = msisdn
    end

    def fetch
      RestClient.get "http://#{@host}/operators/#{@operator}/msisdns/#{@msisdn}", {:"X-ApiKey" => @api_key}
    end

  end

end