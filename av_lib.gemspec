# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'av_lib/version'

Gem::Specification.new do |spec|
  spec.name          = "av_lib"
  spec.version       = AvLib::VERSION
  spec.authors       = ["Jakub Oboza"]
  spec.email         = ["jakub.oboza@gmail.com"]
  spec.summary       = %q{Mgage AV api gem (Age Verification)}
  spec.description   = %q{Mgage Ruby lib to help integrate Age Verification application}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
