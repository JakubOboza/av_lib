require 'spec_helper'

## curl -H "X-ApiKey: 13fae8a8c955f64f67023a8ac681b9ba24a1931b" http://av-app-1:8000/operators/three-uk/msisdns/447740630598

def stub_av(operator, result)
  stub_request(:get, "http://localhost/operators/#{operator}/msisdns/447429150645").with(:headers => {"X-ApiKey" => "some-test-key"}).to_return(:body => "<html><body><dl><dt>Operator</dt><dd>three-uk</dd><dt>MSISDN</dt><dd>447740630598</dd><dt>Is Verified</dt><dd>#{result}</dd></dl></body></html>")
end

def stub_av_timeout(operator)
  stub_request(:get, "http://localhost/operators/#{operator}/msisdns/447429150645").with(:headers => {"X-ApiKey" => "some-test-key"}).to_timeout
end


describe AvLib do

  before(:each) do
    AvLib.setup(host: "localhost", api_key: "some-test-key")
  end


  it "can be configured" do
    lambda do
      AvLib.setup(host: "localhost", api_key: "some-test-key")
    end.should_not raise_error
    expect(AvLib.api_key).to eql("some-test-key")
  end

  it "can perform positive lookup" do
    stub_av("three-uk", "true")
    expect(AvLib.lookup(:three, "447429150645").verified?).to eql(true)
  end

  it "can perform negative lookup" do
    stub_av("orange-uk", "false")
    expect(AvLib.lookup(:orange, "447429150645").verified?).to eql(false)
  end

  it "can get unknown status" do
    stub_av("ee", "unknown")
    expect(AvLib.lookup(:ee, "447429150645").verified?).to eql("unknown")
  end

  describe "time out" do

    it "can handle timeouts" do
      stub_av_timeout("ee")
      expect(AvLib.lookup(:ee, "447429150645").verified?).to eql("unknown")
    end

    it "can force explosion on timeouts" do
      stub_av_timeout("ee")
      lambda do
        AvLib.lookup(:ee, "447429150645", true).verified?
      end.should raise_error
    end

  end

end